using System;
using System.Collections.Generic;
using UnityEngine;

public class BattleTable : MonoBehaviour
{
    #region Singleton

    public static BattleTable Instance;

    private void Awake()
    {
        Instance = this;
    }

    #endregion
    
    public List<Bots> EnabledBots
    {
        get;
        private set;
    } = new List<Bots>();

    private void OnEnable()
    {
        Bots.Spawned += Bots_Spawned; 
        Bots.Destroyed += Bots_Destroyed;
    }

    private void OnDisable()
    {
        Bots.Spawned -= Bots_Spawned;
        Bots.Destroyed -= Bots_Destroyed;
    }

    private void Bots_Spawned(Bots obj)
    {
        EnabledBots.Add(obj);
    }
    
    private void Bots_Destroyed(Bots obj)
    {
        EnabledBots.Remove(obj);
    }


    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.TryGetComponent(out Dice dice))
        {
            dice.CheckDiceVelocity();
        }
    }
}
