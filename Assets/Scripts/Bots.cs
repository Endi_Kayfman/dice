using System;
using System.Collections;
using System.Linq;
using UnityEngine;

public abstract class Bots : MonoBehaviour, IPoolObject
{
    public static event Action<Bots> Spawned = delegate { };
    public static event Action<Bots> Destroyed = delegate { };

    private Coroutine _moveBotCor = null;
    
    public abstract ObjectPooler.ObjectInfo.ObjectType Type { get; }
    protected abstract ObjectPooler.ObjectInfo.ObjectType BotType
    {
        get;
    }
    
    public abstract bool isPlayerBot
    {
        get;
    }

    private void OnEnable()
    {
        Spawned(this);
    }

    private void OnDisable()
    {
        Destroyed(this);
        _moveBotCor = null;
    }

    private void Update()
    {
        _moveBotCor ??= StartCoroutine(MoveBotCor());
    }

    protected void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.TryGetComponent(out Bots bot))
        {
            if (isPlayerBot != bot.isPlayerBot)
            {
                ObjectPooler.Instance.DestroyObject(bot.gameObject);
            }
        }
    }
    
    private IEnumerator MoveBotCor()
    {
        Bots targetBot = BattleTable.Instance.EnabledBots.FirstOrDefault(bot => bot.isPlayerBot != isPlayerBot);

        if (targetBot != null)
        {
            while (targetBot.gameObject.activeSelf)
            {
                Vector3 delta = targetBot.transform.position - transform.position;
                delta.Normalize();
                float moveSpeed = Time.deltaTime;
                transform.position = transform.position + (delta  * moveSpeed);
            
                yield return null;
            }
        }
        
        _moveBotCor = null;
    }
}
