using UnityEngine;

public class Destroyer : MonoBehaviour
{
    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.TryGetComponent(out PlayerDice dicePlayer))
        {
            ObjectPooler.Instance.DestroyObject(dicePlayer.gameObject);
           
        }
        
        if (other.gameObject.TryGetComponent(out EnemyDice diceEnemy))
        {
            ObjectPooler.Instance.DestroyObject(diceEnemy.gameObject);
           
        }

        if (other.gameObject.TryGetComponent(out Bots bot))
        {
            ObjectPooler.Instance.DestroyObject(bot.gameObject);
        }
    }
}
