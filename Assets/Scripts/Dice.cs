using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Collider))]

public abstract class Dice : MonoBehaviour, IPoolObject
{
    public static event Action<Dice> DiceThrowed = delegate { };
    
    [SerializeField] 
    private GameObject[] _diceSides;
    
    protected Rigidbody _rigidbody = null;

    private Vector3 _mousePressDownPos = Vector3.zero;
    private Vector3 _mouseReleasedPos = Vector3.zero;
    
    public abstract bool isPlayerDice
    {
        get;
    }
    
    public abstract ObjectPooler.ObjectInfo.ObjectType Type { get; }
    protected abstract ObjectPooler.ObjectInfo.ObjectType BotType
    {
        get;
    }

    protected virtual void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }

#if UNITY_ANDROID
    private void Update()
    {
        if (isPlayerDice)
        {
            if (Input.touchCount > 0)
            {
                Touch touch = Input.GetTouch(0);

                if (touch.phase == TouchPhase.Began)
                {
                    _mousePressDownPos = touch.position; 
                }
            
                if (touch.phase == TouchPhase.Moved)
                {
                    if (isPlayerDice)
                    {
                        Vector3 forceInit = ((Vector3)touch.position - _mousePressDownPos);
                        Vector3 forceV = (new Vector3(forceInit.x, forceInit.y, forceInit.y));
                        forceV.y = Mathf.Clamp(forceV.y, -Mathf.Infinity, Mathf.Infinity);
                        forceV.z = Mathf.Clamp(forceV.z, -Mathf.Infinity, Mathf.Infinity);
                        forceV = forceV * 2f;
                        DrawTrajectory.Instance.UpdateTrajectory(forceV, _rigidbody, transform.position);
                    }
                }

                if (touch.phase == TouchPhase.Ended)
                {
                    _mouseReleasedPos = touch.position;

                    Vector3 temp = _mousePressDownPos - _mouseReleasedPos;
                    temp.z = Mathf.Clamp(temp.y, -Mathf.Infinity, Mathf.Infinity);
                    temp.y = Mathf.Clamp(temp.y, -Mathf.Infinity, Mathf.Infinity);
                    Shoot(temp * 2f);
                }
            }
        }
   
    }
#endif

#if UNITY_EDITOR
    protected void OnMouseDown()
    {
        if (isPlayerDice)
        {
            _mousePressDownPos = Input.mousePosition;
        }
       
    }
    
    protected void OnMouseUp()
    {
        if (isPlayerDice)
        {
            _mouseReleasedPos = Input.mousePosition;
    
            Vector3 temp = _mousePressDownPos - _mouseReleasedPos;
            temp.z = Mathf.Clamp(temp.y, -Mathf.Infinity, Mathf.Infinity);
            temp.y = Mathf.Clamp(temp.y, -Mathf.Infinity, Mathf.Infinity);
            Shoot(temp * 2f);
        }
    }
    
    protected void OnMouseDrag()
    {
        if (isPlayerDice)
        {
            Vector3 forceInit = (Input.mousePosition - _mousePressDownPos);
            Vector3 forceV = (new Vector3(forceInit.x, forceInit.y, forceInit.y));
            forceV.y = Mathf.Clamp(forceV.y, -Mathf.Infinity, Mathf.Infinity);
            forceV.z = Mathf.Clamp(forceV.z, -Mathf.Infinity, Mathf.Infinity);
            forceV = forceV * 2f;
            DrawTrajectory.Instance.UpdateTrajectory(forceV, _rigidbody, transform.position);
        }
    }
#endif

    protected void Shoot(Vector3 force)
    {
        if (force.y > 0)
        {
            _rigidbody.AddForce(new Vector3(force.x, force.y, force.y));
        }else
        {
            _rigidbody.AddForce(new Vector3(-force.x, Mathf.Abs(force.y), Mathf.Abs(force.y)));
        }
      
        _rigidbody.angularVelocity += new Vector3(Random.Range(0, 50f), Random.Range(0, 50f), Random.Range(0, 50f));
        
        DiceThrowed(this);
    }
    

    public void CheckDiceVelocity()
    {
       StartCoroutine(CheckDiceVelocityCor());
    }
    
    protected IEnumerator CheckDiceVelocityCor()
    {
        while (_rigidbody.velocity.magnitude > 0.0f)
        {
            yield return null;
        }
        
        ToggleSides(true);
    }

    public void SpawnBots(int diceNumber)
    {
        for (int i = 0; i < diceNumber; i++)
        {
            GameObject bot = ObjectPooler.Instance.GetObject(BotType);
            bot.transform.position = transform.position + new Vector3(Random.Range(-1f,1f), 0, Random.Range(-1f,1f));
        }
        
        ToggleSides(false);
        ObjectPooler.Instance.DestroyObject(gameObject);
        DrawTrajectory.Instance.HideLine();
    }

    protected void ToggleSides(bool toggle)
    {
        for (int i = 0; i < _diceSides.Length; i++)
        {
            _diceSides[i].SetActive(toggle);
        }
    }
}
