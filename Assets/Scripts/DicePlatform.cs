using System.Collections;
using UnityEngine;

public abstract class DicePlatform : MonoBehaviour
{
    [SerializeField]
    private float _timeToRespawnDice = 0.0f;

    [SerializeField]
    protected Transform _spawnPoint = null;

    public abstract bool IsPlayerPlatform
    {
        get;
    }
    
    protected abstract ObjectPooler.ObjectInfo.ObjectType DiceType
    {
        get;
    }
    
    private void OnEnable()
    {
        Dice.DiceThrowed += Dice_DiceThrowed;
    }
    
    private void OnDisable()
    {
        Dice.DiceThrowed -= Dice_DiceThrowed;
    }

    protected void Start()
    {
        SpawnDice();
    }
    
    private void Dice_DiceThrowed(Dice dice)
    {
        if (dice.isPlayerDice == IsPlayerPlatform)
        {
            StartCoroutine(SpawnDiceTimerCor());
        }
    }

    protected abstract void SpawnDice();

    private IEnumerator SpawnDiceTimerCor()
    {
        float timer = 0f;

        while (timer <= _timeToRespawnDice)
        {
            timer += Time.deltaTime;
            yield return null;
        }

        SpawnDice();
    }
}
