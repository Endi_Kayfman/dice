using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiceSide : MonoBehaviour
{
   [SerializeField] private int _siteNumber = 0;

   private Dice _dice = null; 
   
   private void Start()
   {
      _dice = GetComponentInParent<Dice>();
   }

   private void OnTriggerEnter(Collider other)
   {
      if (other.TryGetComponent(out BattleTable ground))
      {
         _dice.SpawnBots(_siteNumber);
      }
   }
}
