public class EnemyBots : Bots
{
    public override ObjectPooler.ObjectInfo.ObjectType Type => BotType;
    protected override ObjectPooler.ObjectInfo.ObjectType BotType => ObjectPooler.ObjectInfo.ObjectType.EnemyBots;
    public override bool isPlayerBot => false;
}
