using System;
using System.Collections;
using DG.Tweening;
using UnityEngine;
using Random = UnityEngine.Random;

public class EnemyDice : Dice
{
    public override bool isPlayerDice => false;
    public override ObjectPooler.ObjectInfo.ObjectType Type => ObjectPooler.ObjectInfo.ObjectType.EnemyDice;
    protected override ObjectPooler.ObjectInfo.ObjectType BotType => ObjectPooler.ObjectInfo.ObjectType.EnemyBots;

    private Vector3 point = Vector3.zero;

    private void SelectRandomPoint()
    {
        point = new Vector3(Random.Range(-2.5f, 2.6f), 5.1f, Random.Range(-5f, 5f));
    }
    
    private void OnEnable()
    {
        StartCoroutine(ShootDice());
    }

    private IEnumerator ShootDice()
    {
        SelectRandomPoint();
        
        yield return new WaitForSeconds(4);
       
            Shoot(point);
            transform.DOJump(point, 1, 1, 1);
    }
}
