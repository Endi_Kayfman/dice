using System;
using UnityEngine;

public class EnemyPlatform : DicePlatform
{
    public override bool IsPlayerPlatform => false;
    protected override ObjectPooler.ObjectInfo.ObjectType DiceType => ObjectPooler.ObjectInfo.ObjectType.EnemyDice;

    protected override void SpawnDice()
    {
        GameObject dice = ObjectPooler.Instance.GetObject(DiceType);
        dice.transform.position = _spawnPoint.position;
    }
}
