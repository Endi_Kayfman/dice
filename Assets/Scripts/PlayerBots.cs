public class PlayerBots : Bots
{
    public override ObjectPooler.ObjectInfo.ObjectType Type => BotType;
    protected override ObjectPooler.ObjectInfo.ObjectType BotType => ObjectPooler.ObjectInfo.ObjectType.PlayerBots;
    public override bool isPlayerBot => true;
}
