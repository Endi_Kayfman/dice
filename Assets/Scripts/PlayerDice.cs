using UnityEngine;

public class PlayerDice : Dice
{
    public override bool isPlayerDice => true;
    public override ObjectPooler.ObjectInfo.ObjectType Type => ObjectPooler.ObjectInfo.ObjectType.PlayerDice;
    protected override ObjectPooler.ObjectInfo.ObjectType BotType => ObjectPooler.ObjectInfo.ObjectType.PlayerBots;
    
}
