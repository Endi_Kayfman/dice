using System;
using UnityEditor;
using UnityEngine;

public class PlayerPlatform : DicePlatform
{
    public override bool IsPlayerPlatform => true;
    protected override ObjectPooler.ObjectInfo.ObjectType DiceType => ObjectPooler.ObjectInfo.ObjectType.PlayerDice;

    protected override void SpawnDice()
    {
        GameObject dice = ObjectPooler.Instance.GetObject(DiceType);
        dice.transform.position = _spawnPoint.position;
        dice.transform.rotation = Quaternion.Euler(Vector3.zero);
    }
}
