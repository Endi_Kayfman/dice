using System.Collections;
using System.Collections.Generic;
using UnityEngine;

interface IPoolObject
{
    ObjectPooler.ObjectInfo.ObjectType Type { get; }
}
